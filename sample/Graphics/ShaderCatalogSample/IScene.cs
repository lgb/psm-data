/* PlayStation(R)Mobile SDK 2.00.00
 * Copyright (C) 2014 Sony Computer Entertainment Inc.
 * All Rights Reserved.
 */
using System;
using Sce.PlayStation.Core;
using Sce.PlayStation.Core.Graphics;

namespace Sample
{

interface IScene
{
    string Name
    {
        get;
    }
    void Setup( GraphicsContext graphics, Model model );
    void Dispose();
    void Update( float delta );
    void Render( GraphicsContext graphics, Camera camera, LightModel light, Model model, BgModel bg );
}

}
