/* PlayStation(R)Mobile SDK 2.00.00
 * Copyright (C) 2014 Sony Computer Entertainment Inc.
 * All Rights Reserved.
 */

using System ;
using System.Threading ;
using System.Diagnostics ;
using System.Collections.Generic ;
using System.Runtime.InteropServices;
using Sce.PlayStation.Core ;
using Sce.PlayStation.Core.Graphics ;
using Sce.PlayStation.Core.Imaging ;

namespace Sample
{

//----------------------------------------------------------------
//	Sprite Text
//----------------------------------------------------------------

public class SpriteText : Sprite
{
	public SpriteText( SpriteTextMaterial material, int zIndex = 0 ) : base( material, zIndex )
	{
		textString = "" ;
		textSprites = new Sprite[ 0 ] ;		// Automatically extended

		Center = Vector2.Zero ;		// Top-left anchor
	}

	//  Functions

	public new void UpdateAll()
	{
		UpdatePosition() ;
		UpdateTexCoord() ;
		UpdateColor() ;
	}
	public new void UpdatePosition()
	{
		var material = base.material as SpriteTextMaterial ;
		float w = textWidth * Scale.X ;
		float h = textHeight * Scale.Y ;
		Vector2 pos, dir, offset ;
		Sprite.CosSin( Direction, out dir ) ;
		offset.X = w * Center.X ;
		offset.Y = h * ( Center.Y - 0.5f ) ;
		pos.X = Position.X - dir.X * offset.X + dir.Y * offset.Y ;
		pos.Y = Position.Y - dir.Y * offset.X - dir.X * offset.Y ;
		dir.X *= Scale.X * ( 1.0f / 16.0f ) ;
		dir.Y *= Scale.Y * ( 1.0f / 16.0f ) ;
		ushort prevW = 0 ;
		for ( int i = 0 ; i < textLength ; i ++ ) {
			var info = material.GetCharInfo( textString[ i ] ) ;
			float advance = ( prevW + info.W ) * 0.5f ;
			pos.X += dir.X * advance ;
			pos.Y += dir.Y * advance ;
			prevW = info.W ;
			var sprite = textSprites[ i ] ;
			sprite.Position.X = pos.X ;
			sprite.Position.Y = pos.Y ;
			sprite.Direction = Direction ;
			sprite.Size.X = info.Z * Scale.X ;
			sprite.Size.Y = h ;
			sprite.UpdatePosition() ;
		}
	}
	public new void UpdateColor()
	{
		for ( int i = 0 ; i < textString.Length ; i ++ ) {
			var sprite = textSprites[ i ] ;
			sprite.Color = Color ;
			sprite.UpdateColor() ;
		}
	}
	public Sprite GetCharSprite( int index )
	{
		return textSprites[ index ] ;
	}

	//	Params

	public string Text {
		get { return textString ; }
		set { if ( textString != value ) { textString = value ; UpdateTextParam() ; } }
	}
	public float TextWidth {
		get { return textWidth ; }
	}
	public float TextHeight {
		get { return textHeight ; }
	}

	//	Overrides

	public override SpriteBatch Batch {
		get { return base.Batch ; }
		set {
			base.Batch = value ;
			int left = ( value == null ) ? 0 : value.SpriteCapacity - value.SpriteCount ;
			for ( int i = 0 ; i < textCapacity ; i ++ ) textSprites[ i ].Batch = ( i >= left ) ? null : value ;
		}
	}
	public override SpriteMaterial Material {
		get { return base.Material ; }
		set {
			base.Material = value ;
			for ( int i = 0 ; i < textCapacity ; i ++ ) textSprites[ i ].Material = value ;
		}
	}
	public override int ZIndex {
		get { return base.ZIndex ; }
		set { 
			base.ZIndex = value ;
			for ( int i = 0 ; i < textCapacity ; i ++ ) textSprites[ i ].ZIndex = value ;
		}
	}

	//  Subroutines

	internal void UpdateTextParam()
	{
		var material = base.material as SpriteTextMaterial ;
		material.AddCharSet( textString ) ;

		//  Add new sprites

		if ( textString.Length > textCapacity ) {
			var sprites = new Sprite[ textString.Length ] ;
			Array.Copy( textSprites, sprites, textCapacity ) ;
			textSprites = sprites ;
			int left = ( batch == null ) ? 0 : textCapacity + batch.SpriteCapacity - batch.SpriteCount ;
			for ( int i = textCapacity ; i < textString.Length ; i ++ ) {
				var sprite = new Sprite( material, zIndex ) ;
				if ( i < left ) batch.AddSprite( sprite ) ;
				textSprites[ i ] = sprite ;
			}
			textCapacity = textString.Length ;
		}

		//  Set texcoords

		textHeight = material.FontHeight ;
		textWidth = 0.0f ;
		for ( int i = 0 ; i < textString.Length ; i ++ ) {
			var info = material.GetCharInfo( textString[ i ] ) ;
			var sprite = textSprites[ i ] ;
			sprite.UVOffset.X = info.X ;
			sprite.UVOffset.Y = info.Y ;
			sprite.UVSize.X = info.Z ;		// Width
			sprite.UVSize.Y = textHeight ;
			sprite.UpdateTexCoord() ;
			textWidth += info.W / 16.0f ;	// HorizontalAdvance
		}

		//  Hide unused sprites

		for ( int i = textString.Length ; i < textLength ; i ++ ) {
			var sprite = textSprites[ i ] ;
			sprite.Size = Vector2.Zero ;
			sprite.UpdatePosition() ;
		}
		textLength = textString.Length ;
	}

	//	Params

	string textString ;
	Sprite[] textSprites ;
	int textCapacity ;
	int textLength ;
	float textWidth ;
	float textHeight ;
}

//----------------------------------------------------------------
//	Sprite Text Material
//----------------------------------------------------------------

public class SpriteTextMaterial : SpriteMaterial
{
	public SpriteTextMaterial( Font font ) : base( null )
	{
		ShaderProgram = DefaultShaderProgram ;

		this.font = font ;
		fontHeight = font.Metrics.Height ;
		fontMap = new UShort4[ 256 ] ;		// Automatically extended
		lineImages = new Image[ 32 ] ;
		lineCount = 0 ;
		lineWidth = 0 ;

		//  Initial char set
		string ascii = " !\"#$%&'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]^_`abcdefghijklmnopqrstuvwxyz{|}~";
		AddCharSet( ascii ) ;
	}
	public new void Dispose()
	{
		Dispose( true ) ;
	}
	protected override void Dispose( bool disposing )
	{
		if ( disposing ) {
			for ( int i = 0 ; i < lineCount ; i ++ ) lineImages[ i ].Dispose() ;
			lineImages = null ;
			fontMap = null ;
		}
		base.Dispose( disposing ) ;
	}

	//  Functions

	public UShort4 GetCharInfo( char c )
	{
		return fontMap[ c ] ;
	}
	public void AddCharSet( string charSet )
	{
		//  Font map image

		CharMetrics[] cmxs = null ;
		int maxWidth = 2048 ;
		int oldLineCount = lineCount ;
		bool needUpdate = false ;
		for ( int i = 0 ; i < charSet.Length ; i ++ ) {
			char c = charSet[ i ] ;
			if ( c >= fontMap.Length ) {
				var map = new UShort4[ 65536 ] ;
				Array.Copy( fontMap, map, fontMap.Length ) ;
				fontMap = map ;
			}
			if ( fontMap[ c ].Z != 0 ) continue ;
			if ( cmxs == null ) cmxs = font.GetTextMetrics( charSet ) ;
			var cmx = cmxs[ i ] ;
			int charWidth = (int)Math.Floor( Math.Max( cmx.HorizontalBearingX + cmx.Width, cmx.HorizontalAdvance ) ) ;
			if ( lineCount == 0 || lineWidth + charWidth > maxWidth ) {
				lineImages[ lineCount ] = new Image( ImageMode.A, new ImageSize( maxWidth, fontHeight ), new ImageColor( 0, 0, 0, 0 ) ) ;
				lineWidth = 0 ;
				lineCount ++ ;
			}
			var image = lineImages[ lineCount - 1 ] ;
			image.DrawText( c.ToString(), new ImageColor( 255, 255, 255, 255 ), font, new ImagePosition( lineWidth, 0 ) ) ;
			fontMap[ c ] = new UShort4( (ushort)lineWidth, (ushort)( fontHeight * ( lineCount - 1 ) ),
										(ushort)charWidth, (ushort)( cmx.HorizontalAdvance * 16.0f ) ) ;
			lineWidth += charWidth ;
			needUpdate = true ;
		}
		if ( !needUpdate ) return ;

		//  Font map texture

		if ( lineCount != oldLineCount ) {
			if ( Texture != null && Texture != DefaultTexture ) Texture.Dispose() ;
			Texture = new Texture2D( maxWidth, fontHeight * lineCount, false, PixelFormat.Alpha ) ;
			oldLineCount = 0 ;
			var uvScale = new Vector2( 1.0f / Texture.Width, 1.0f / Texture.Height ) ;
			ShaderProgram.SetUniformValue( 2, ref uvScale ) ;
		}

		//  Update pixels

		int lineStart = ( oldLineCount == 0 ) ? 0 : oldLineCount - 1 ;
		for ( int i = lineStart ; i < lineCount ; i ++ ) {
			Texture.SetPixels( 0, lineImages[ i ].ToBuffer(), PixelFormat.Alpha, 0, maxWidth, 0, fontHeight * i, maxWidth, fontHeight ) ;
		}
	}

	//	Params

	public Font Font {
		get { return font ; }
	}
	public int FontHeight {
		get { return fontHeight ; }
	}

	//	Internal params

	internal Font font ;
	internal int fontHeight ;
	internal UShort4[] fontMap ;	// X Y Width HorizontalAdvance
	internal Image[] lineImages ;
	internal int lineCount ;
	internal int lineWidth ;

	//	Default values

	public static new ShaderProgram DefaultShaderProgram {
		get {
			if ( defaultShaderProgram == null ) {
				var filename = "/Application/shaders/SpriteText.cgx" ;
				defaultShaderProgram = new ShaderProgram( filename ) ;
			}
			return defaultShaderProgram ;
		}
	}

	internal static new ShaderProgram defaultShaderProgram ;
}


} //  end ns Sample
