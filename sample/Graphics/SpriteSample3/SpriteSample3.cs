﻿/* PlayStation(R)Mobile SDK 2.00.00
 * Copyright (C) 2014 Sony Computer Entertainment Inc.
 * All Rights Reserved.
 */
using System ;
using System.Threading ;
using System.Diagnostics ;
using Sce.PlayStation.Core ;
using Sce.PlayStation.Core.Graphics ;
using Sce.PlayStation.Core.Environment ;
using Sce.PlayStation.Core.Imaging ;
using Sce.PlayStation.Core.Input ;

namespace Sample
{

/**
 * SpriteSample3
 */
class SpriteSample3
{
	static GraphicsContext graphics ;
	static Rectangle boundary ;
	static Random random ;

	static SpriteBatch batch ;
	static SpriteTextMaterial material ;
	static SpriteText[] texts ;
	static int textCount ;

	static float textScale = 1.0f ;
	static bool textAnim = true ;

	static Rgba[] colors = {
		new Rgba(0,0,0,255), new Rgba(224,0,0,255), new Rgba(0,224,0,255), new Rgba(224,224,0,255), 
		new Rgba(0,0,224,255), new Rgba(224,0,224,255), new Rgba(0,224,224,255), new Rgba(224,224,224,255)
	} ;
	static string[] words = {
		"above-mentioned",	"congratulations",	"disillusionment",	"extraordinarily",
		"four over three",	"hospitalization",	"psychologically",	"straightforward"
	} ;

	static bool loop = true ;

	static void Main( string[] args )
	{
		Init() ;
		while ( loop ) {
			SystemEvents.CheckEvents() ;
			Update() ;
			Render() ;
		}
		Term() ;
	}

	static bool Init()
	{
		graphics = new GraphicsContext() ;
		boundary = new Rectangle( 32, 32, graphics.Screen.Width - 64, graphics.Screen.Height - 64 ) ;
		random = new Random() ;
		SampleDraw.Init( graphics ) ;
		SampleTimer.Init() ;

		InitSpriteBatch( 4096, 256 ) ;
		SetTextCount( 256 ) ;
		return true ;
	}

	static void Term()
	{
		SampleDraw.Term() ;
		SampleTimer.Term() ;
		batch.Dispose() ;
		material.Dispose() ;
		graphics.Dispose() ;
	}

	static bool Update()
	{
		SampleDraw.Update() ;

		var padData = GamePad.GetData( 0 ) ;
		var press = padData.ButtonsDown ;
		if ( press != 0 ) {
			if ( ( GamePadButtons.Left & press ) != 0 ) SetTextCount( textCount * 2 ) ;
			if ( ( GamePadButtons.Right & press ) != 0 ) SetTextCount( textCount / 2 ) ;
			if ( ( GamePadButtons.Up & press ) != 0 ) SetTextScale( textScale + 0.2f ) ;
			if ( ( GamePadButtons.Down & press ) != 0 ) SetTextScale( textScale - 0.2f ) ;
			if ( ( GamePadButtons.Circle & press ) != 0 ) textAnim = !textAnim ;
		}
		return true ;
	}

	static bool Render()
	{
		SampleTimer.StartFrame() ;

		graphics.SetViewport( 0, 0, graphics.Screen.Width, graphics.Screen.Height ) ;
		graphics.SetClearColor( 0.0f, 0.5f, 1.0f, 0.0f ) ;
		graphics.Clear() ;

		UpdateWords() ;

		for ( int i = 0 ; i < textCount ; i ++ ) {
			MoveText( texts[ i ], i, SampleTimer.DeltaTime ) ;
		}
		batch.Draw( graphics ) ;

		SampleDraw.DrawText( "Sprite Sample 3", 0xffffffff, 0, 0 ) ;
		var msg = string.Format( "SpriteCount {0:D4} / FrameRate {1:F2} fps / FrameTime {2:F2} ms",
					batch.SpriteCount, SampleTimer.AverageFrameRate, SampleTimer.AverageFrameTime * 1000 ) ;
		SampleDraw.DrawText( msg, 0xffffffff, 0, graphics.Screen.Height - 24 ) ;

		SampleTimer.EndFrame() ;
		graphics.SwapBuffers() ;
		return true ;
	}

	//------------------------------------------------------------
	//  Sprite functions
	//------------------------------------------------------------

	static void InitSpriteBatch( int maxSpriteCount, int maxTextCount )
	{
		batch = new SpriteBatch( maxSpriteCount ) ;
		material = new SpriteTextMaterial( new Font( FontAlias.System, 24, FontStyle.Regular ) ) ;
		material.Texture.SetFilter( TextureFilterMode.Linear, TextureFilterMode.Linear,
									TextureFilterMode.Nearest ) ;

		texts = new SpriteText[ maxTextCount ] ;
		textCount = 0 ;
	}

	static void SetTextCount( int count )
	{
		count = Math.Min( Math.Max( count, 1 ), texts.Length ) ;
		if ( count > textCount ) {
			for ( int i = textCount ; i < count ; i ++ ) {
				texts[ i ] = new SpriteText( material ) ;
				InitText( texts[ i ], i ) ;
				if ( batch.SpriteCount < batch.SpriteCapacity ) batch.AddSprite( texts[ i ] ) ;
			}
		} else {
			for ( int i = count ; i < textCount ; i ++ ) {
				if ( texts[ i ].Batch == batch ) batch.RemoveSprite( texts[ i ] ) ;
			}
		}
		textCount = count ;
	}

	static void SetTextScale( float scale )
	{
		scale = FMath.Clamp( scale, 0.2f, 16.0f ) ;
		for ( int i = 0 ; i < textCount ; i ++ ) {
			texts[ i ].Scale.X = texts[ i ].Scale.Y = scale ;
		}
		textScale = scale ;
	}

	static void InitText( SpriteText t, int index )
	{
		t.Position.X = boundary.X + boundary.Width * (float)random.NextDouble() ;
		t.Position.Y = boundary.Y + boundary.Height * (float)random.NextDouble() ;
		t.Direction = FMath.Radians( random.Next( 360 ) ) ;
		t.Rotation = FMath.Radians( random.Next( -36, 36 ) ) ;
		t.Scale = new Vector2( textScale, textScale ) ;
		t.Center = new Vector2( 0.5f, 0.5f ) ;
		t.Color = colors[ index % 8 ] ;
		t.Text = words[ index % 8 ] ;

		t.UpdateAll() ;
	}

	static void MoveText( SpriteText t, int index, float delta )
	{
		t.Direction += t.Rotation * delta ;

		if ( textAnim ) t.Text = words[ index % 8 ] ;

		t.UpdatePosition() ;
	}

	//------------------------------------------------------------
	//  Subroutines
	//------------------------------------------------------------

	static void UpdateWords()
	{
		//  Uppercase some letters

		for ( int i = 0 ; i < words.Length ; i ++ ) {
			var word = words[ i ] ;
			int n = word.Length ;
			int x = ( SampleTimer.FrameCount / 2 + i ) % ( n * 2 ) ;
			int a = ( x < n ) ? 0 : x - n ;
			int b = ( x > n ) ? n : x ;
			words[ i ] = word.Substring( 0, a ).ToLower() + word.Substring( a, b - a ).ToUpper() + word.Substring( b, n - b ).ToLower() ;
		}
	}
}


} // end ns Sample
