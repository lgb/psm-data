/* PlayStation(R)Mobile SDK 2.00.00
 * Copyright (C) 2014 Sony Computer Entertainment Inc.
 * All Rights Reserved.
 */

#define RESIZE_VERTEX_BUFFER
#define ENABLE_SIN_TABLE

using System ;
using System.Threading ;
using System.Diagnostics ;
using System.Runtime.InteropServices;
using Sce.PlayStation.Core ;
using Sce.PlayStation.Core.Graphics ;

namespace Sample
{

//----------------------------------------------------------------
//  Sprite Batch
//----------------------------------------------------------------

public class SpriteBatch : Sprite, IDisposable
{
	public SpriteBatch( int maxSpriteCount, int zIndex = 0 ) : base( null, zIndex )
	{
		if ( maxSpriteCount > 65535 ) {
			throw new ArgumentOutOfRangeException( "maxSpriteCount > 65535" ) ;
		}

		#if !RESIZE_VERTEX_BUFFER
		vertexBuffer = new VertexBuffer( maxSpriteCount, 0, 1, vertexFormats ) ;
		#endif // RESIZE_VERTEX_BUFFER
		vertexData = new Vertex[ maxSpriteCount ] ;

		float[] points = { 0, 0, 0, 1, 1, 0, 1, 1 } ;
		vertexBuffer2 = new VertexBuffer( 4, VertexFormat.Float2 ) ;
		vertexBuffer2.SetVertices( 0, points ) ;

		sortedList = new Sprite[ maxSpriteCount ] ;
		spriteCapacity = maxSpriteCount ;

		Center = Vector2.Zero ;		// Top-left anchor
	}
	public void Dispose()
	{
		Dispose( true ) ;
	}
	protected virtual void Dispose( bool disposing )
	{
		if ( disposing ) {
			if ( vertexBuffer != null ) vertexBuffer.Dispose() ;
			if ( vertexBuffer2 != null ) vertexBuffer2.Dispose() ;
			vertexBuffer = null ;
			vertexBuffer2 = null ;
			vertexData = null ;
			sortedList = null ;
		}
	}

	//  Functions

	public override void Draw( GraphicsContext graphics )
	{
		if ( spriteCount == 0 ) return ;
		#if RESIZE_VERTEX_BUFFER
		ResizeBuffer() ;
		#endif // RESIZE_VERTEX_BUFFER
		SortVertexData() ;
		CopyVertexData() ;
		DrawSprites( graphics ) ;
	}
	public void AddSprite( Sprite sprite )
	{
		if ( sprite.batch != null ) sprite.batch.RemoveSprite( sprite ) ;
		if ( spriteCount >= spriteCapacity ) throw new ArgumentOutOfRangeException() ;
		AddToSortedList( sprite ) ;
		spriteCount ++ ;
		sprite.batch = this ;		// Suppress recursion
		sprite.Batch = this ;		// Virtual property
		if ( sprite.DrawTypeID == 0 ) sprite.UpdateAll() ;
	}
	public void RemoveSprite( Sprite sprite )
	{
		if ( sprite.batch != this ) throw new InvalidOperationException() ;
		RemoveFromSortedList( sprite ) ;
		spriteCount -- ;
		sprite.batch = null ;		// Suppress recursion
		sprite.Batch = null ;		// Virtual property
	}

	//  Params

	public int SpriteCapacity {
		get { return spriteCapacity ; }
	}
	public int SpriteCount {
		get { return spriteCount ; }
	}

	//  Overrides

	public override byte DrawTypeID {
		get { return 1 ; }
	}

	//  Subroutines

	#if RESIZE_VERTEX_BUFFER
	internal void ResizeBuffer()
	{
		int oldCapacity = bufferCapacity ;
		if ( spriteCount > bufferCapacity ) {
			int threshold = (int)( bufferCapacity * bufferResizeFactor ) ;
			bufferCapacity = spriteCount + bufferResizeMargin ;
			if ( bufferCapacity < threshold ) bufferCapacity = threshold ;
			if ( bufferCapacity > spriteCapacity ) bufferCapacity = spriteCapacity ;
			bufferReducePending = 0 ;
			bufferReduceTarget = 0 ;
		} else {
			if ( spriteCount > bufferReduceTarget ) bufferReduceTarget = spriteCount ;
			if ( ++ bufferReducePending >= bufferReduceWait ) {
				int threshold = (int)( bufferCapacity / bufferResizeFactor ) ;
				if ( bufferReduceTarget + bufferResizeMargin * 2 <= threshold ) {
					bufferCapacity = bufferReduceTarget + bufferResizeMargin ;
				}
				bufferReducePending = 0 ;
				bufferReduceTarget = 0 ;
			}
		}
		if ( bufferCapacity != oldCapacity ) {
			if ( vertexBuffer != null ) vertexBuffer.Dispose() ;
			vertexBuffer = new VertexBuffer( bufferCapacity, 0, 1, vertexFormats ) ;
			needCopyVertexData = true ;
		}
	}
	#endif // RESIZE_VERTEX_BUFFER
	internal void CopyVertexData()
	{
		if ( !needCopyVertexData ) return ;
		needCopyVertexData = false ;
		vertexBuffer.SetVertices( vertexData, 0, 0, spriteCount ) ;
	}
	internal void SortVertexData()
	{
		if ( !needSortVertexData ) return ;
		needSortVertexData = false ;
		needCopyVertexData = true ;

		int vertexID = 0 ;
		var sprite = sortedList[ 0 ] ;
		for ( int i = 0 ; i < spriteCount ; i ++ ) {
			if ( sprite.vertexID != vertexID ) {
				sprite.vertexID = vertexID ;
				sprite.UpdateVertex( out vertexData[ vertexID ] ) ;
			}
			sprite = sprite.sortNext ;
			vertexID ++ ;
		}
	}
	internal void DrawSprites( GraphicsContext graphics )
	{
		Matrix4 wvp ; Vector4 col ;
		GetTransform( graphics.GetFrameBuffer(), out wvp, out col ) ;

		graphics.Enable( EnableMode.Blend ) ;
		graphics.SetVertexBuffer( 0, vertexBuffer ) ;
		graphics.SetVertexBuffer( 1, vertexBuffer2 ) ;

		Sprite start = sortedList[ 0 ] ;
		for ( int i = 1 ; i <= sortedCount ; i ++ ) {
			var next = sortedList[ i % sortedCount ] ;
			if ( (uint)start.sortKey != (uint)next.sortKey || i == sortedCount ) {
				if ( start.DrawTypeID != 0 ) {
					//  Custom drawing
					do { start.Draw( graphics ) ; start = start.sortNext ; } while ( start != next ) ;
					graphics.Enable( EnableMode.Blend ) ;
					graphics.SetVertexBuffer( 0, vertexBuffer ) ;
					graphics.SetVertexBuffer( 1, vertexBuffer2 ) ;
					continue ;
				}
				var material = start.material ;
				int nextID = ( i < sortedCount ) ? next.vertexID : spriteCount ;
				material.ShaderProgram.SetUniformValue( 0, ref wvp ) ;
				material.ShaderProgram.SetUniformValue( 1, ref col ) ;
				graphics.SetShaderProgram( material.ShaderProgram ) ;
				graphics.SetBlendFunc( material.BlendFunc ) ;
				graphics.SetTexture( 0, material.Texture ) ;
				graphics.DrawArraysInstanced( DrawMode.TriangleStrip, 0, 4, start.vertexID, nextID - start.vertexID ) ;
				start = next ;
			}
		}
	}

	//  Subroutines

	internal void AddToSortedList( Sprite sprite )
	{
		int index = FindSortedList( sprite ) ;
		if ( index < 0 || sortedList[ index ].sortKey != sprite.sortKey ) {
			int count = ( sortedCount ++ ) - ( ++ index ) ;
			if ( count > 0 ) Array.Copy( sortedList, index, sortedList, index + 1, count ) ;
			sortedList[ index ] = sprite ;
			sprite.sortPrev = sprite ;
			sprite.sortNext = sprite ;
		}
		var next = sortedList[ ( index + 1 ) % sortedCount ] ;
		var prev = next.sortPrev ;
		next.sortPrev = sprite ;
		prev.sortNext = sprite ;
		sprite.sortPrev = prev ;
		sprite.sortNext = next ;
		sprite.vertexID = spriteCount ;
		needSortVertexData = true ;
	}
	internal void RemoveFromSortedList( Sprite sprite )
	{
		int index = FindSortedList( sprite ) ;
		if ( sortedList[ index ] == sprite ) {
			sortedList[ index ] = sprite.sortNext ;
			if ( sprite.sortNext == sortedList[ ( index + 1 ) % sortedCount ] ) {
				int count = ( -- sortedCount ) - index ;
				if ( count > 0 ) Array.Copy( sortedList, index + 1, sortedList, index, count ) ;
				sortedList[ sortedCount ] = null ;
			}
		}
		sprite.sortPrev.sortNext = sprite.sortNext ;
		sprite.sortNext.sortPrev = sprite.sortPrev ;
		needSortVertexData = true ;
	}
	internal int FindSortedList( Sprite sprite )
	{
		int lower = -1 ;
		int upper = sortedCount ;
		while ( lower + 1 < upper ) {
			int middle = ( lower + upper ) / 2 ;
			if ( sortedList[ middle ].sortKey > sprite.sortKey ) {
				upper = middle ;
			} else {
				lower = middle ;
			}
		}
		return lower ;
	}

	//  Internal params

	internal VertexBuffer vertexBuffer ;
	internal VertexBuffer vertexBuffer2 ;
	internal Vertex[] vertexData ;
	internal bool needCopyVertexData ;
	internal bool needSortVertexData ;

	internal Sprite[] sortedList ;
	internal int spriteCapacity ;
	internal int spriteCount ;
	internal int sortedCount ;

	#if RESIZE_VERTEX_BUFFER
	const float bufferResizeFactor = 1.5f ;
	const int bufferResizeMargin = 256 ;
	const int bufferReduceWait = 30 ;
	internal int bufferReducePending ;
	internal int bufferReduceTarget ;
	internal int bufferCapacity ;
	#endif // RESIZE_VERTEX_BUFFER

	//  Vertex format

	internal static VertexFormat[] vertexFormats = {
		VertexFormat.Float3,	// Position Direction
		VertexFormat.Float4,	// Size     Center
		VertexFormat.Float4,	// UVOffset UVSize
		VertexFormat.UByte4N	// Color
	} ;
	[StructLayout(LayoutKind.Explicit,Size=48)]
	internal struct Vertex {
		[FieldOffset(0)]
		public Vector2 Position ;
		[FieldOffset(8)]
		public float Direction ;
		[FieldOffset(12)]
		public Vector2 Size ;
		[FieldOffset(20)]
		public Vector2 Center ;
		[FieldOffset(28)]
		public Vector2 UVOffset ;
		[FieldOffset(36)]
		public Vector2 UVSize ;
		[FieldOffset(44)]
		public Rgba Color ;
	}
}

//----------------------------------------------------------------
//  Sprite Material
//----------------------------------------------------------------

public class SpriteMaterial : IDisposable
{
	public SpriteMaterial( Texture2D texture = null )
	{
		MaterialID = ( nextMaterialID ++ ) & 0x00ffffff ;
		ShaderProgram = DefaultShaderProgram ;
		BlendFunc = DefaultBlendFunc ;
		Texture = texture ?? DefaultTexture ;
	}
	public void Dispose()
	{
		Dispose( true ) ;
	}
	protected virtual void Dispose( bool disposing )
	{
		if ( disposing ) {
			if ( ShaderProgram != null && ShaderProgram != defaultShaderProgram ) ShaderProgram.Dispose() ;
			if ( Texture != null && Texture != defaultTexture ) Texture.Dispose() ;
			ShaderProgram = null ;
			Texture = null ;
		}
	}

	//  Params

	public readonly uint MaterialID ;
	public ShaderProgram ShaderProgram ;
	public BlendFunc BlendFunc ;
	public Texture2D Texture ;

	//  Default values

	public static SpriteMaterial DefaultMaterial {
		get {
			if ( defaultMaterial == null ) {
				defaultMaterial = new SpriteMaterial() ;
			}
			return defaultMaterial ;
		}
	}
	public static ShaderProgram DefaultShaderProgram {
		get {
			if ( defaultShaderProgram == null ) {
				var filename = "/Application/shaders/SpriteBatch2.cgx" ;
				defaultShaderProgram = new ShaderProgram( filename ) ;
			}
			return defaultShaderProgram ;
		}
	}
	public static BlendFunc DefaultBlendFunc {
		get {
			return new BlendFunc( BlendFuncMode.Add,
				BlendFuncFactor.SrcAlpha, BlendFuncFactor.OneMinusSrcAlpha ) ;
		}
	}
	public static Texture2D DefaultTexture {
		get {
			if ( defaultTexture == null ) {
				defaultTexture = new Texture2D( 1, 1, false, PixelFormat.Rgba ) ;
				Rgba[] pixels = { new Rgba( 255, 255, 255, 255 ) } ;
				defaultTexture.SetPixels( 0, pixels ) ;
			}
			return defaultTexture ;
		}
	}

	internal static uint nextMaterialID ;
	internal static SpriteMaterial defaultMaterial ;
	internal static ShaderProgram defaultShaderProgram ;
	internal static Texture2D defaultTexture ;
}

//----------------------------------------------------------------
//  Sprite
//----------------------------------------------------------------

public class Sprite
{
	public Sprite( SpriteMaterial material = null, int zIndex = 0 )
	{
		this.material = material ?? SpriteMaterial.DefaultMaterial ;
		this.zIndex = zIndex ;
		UpdateSortKey() ;

		Scale = new Vector2( 1.0f ) ;
		Center = new Vector2( 0.5f ) ;
		UVSize = new Vector2( 1.0f ) ;
		Color = new Rgba( 255, 255, 255, 255 ) ;

		#if ENABLE_SIN_TABLE
		if ( sinTable == null ) {
			sinTable = new float[ 4096 ] ;
			for ( int i = 0 ; i < 4096 ; i ++ ) {
				sinTable[ i ] = FMath.Sin( i * ( FMath.PI / 2048.0f ) ) ;
			}
		}
		#endif // ENABLE_SIN_TABLE
	}

	//  Vertex update

	public void UpdateAll()
	{
		if ( batch == null ) return ;
		UpdateVertex( out batch.vertexData[ vertexID ] ) ;
		batch.needCopyVertexData = true ;
	}
	public void UpdatePosition()
	{
		if ( batch == null ) return ;
		UpdatePosition( ref batch.vertexData[ vertexID ] ) ;
		batch.needCopyVertexData = true ;
	}
	public void UpdateTexCoord()
	{
		if ( batch == null ) return ;
		var vertexData = batch.vertexData ;
		vertexData[ vertexID ].UVOffset = UVOffset ;
		vertexData[ vertexID ].UVSize = UVSize ;
		batch.needCopyVertexData = true ;
	}
	public void UpdateColor()
	{
		if ( batch == null ) return ;
		var vertexData = batch.vertexData ;
		vertexData[ vertexID ].Color = Color ;
		batch.needCopyVertexData = true ;
	}

	//  Custom drawing

	public virtual void Draw( GraphicsContext graphics )
	{
		throw new NotSupportedException() ;		// Custom drawing function
	}
	public virtual byte DrawTypeID {
		get { return 0 ; }						// Custom drawing type-id
	}
	public void GetTransform( FrameBuffer fbuffer, out Matrix4 wvp, out Vector4 col )
	{
		var sprite = this ;

		//  World matrix

		Vector2 cs ;
		CosSin( sprite.Direction, out cs ) ;
		col = sprite.Color.ToVector4() ;
		wvp = Matrix4.Identity ;
		wvp.M11 = cs.X * sprite.Scale.X ;	wvp.M12 = cs.Y * sprite.Scale.X ;
		wvp.M21 = -cs.Y * sprite.Scale.Y ;	wvp.M22 = cs.X * sprite.Scale.Y ;
		wvp.M41 = sprite.Position.X - wvp.M11 * sprite.Center.X - wvp.M21 * sprite.Center.Y ;
		wvp.M42 = sprite.Position.Y - wvp.M12 * sprite.Center.X - wvp.M22 * sprite.Center.Y ;

		//  Parent matrix

		while ( ( sprite = sprite.batch ) != null ) {
			CosSin( sprite.Direction, out cs ) ;
			col *= sprite.Color.ToVector4() ;
			float m11 = cs.X * sprite.Scale.X ;	float m12 = cs.Y * sprite.Scale.X ;
			float m21 = -cs.Y * sprite.Scale.Y ;	float m22 = cs.X * sprite.Scale.Y ;
			float M11 = wvp.M11 ;				float M12 = wvp.M12 ;
			float M21 = wvp.M21 ;				float M22 = wvp.M22 ;
			float M41 = wvp.M41 ;				float M42 = wvp.M42 ;
			wvp.M11 = m11 * M11 + m21 * M12 ;	wvp.M12 = m12 * M11 + m22 * M12 ;
			wvp.M21 = m11 * M21 + m21 * M22 ;	wvp.M22 = m12 * M21 + m22 * M22 ;
			wvp.M41 = m11 * M41 + m21 * M42 ;	wvp.M42 = m12 * M41 + m22 * M42 ;
			wvp.M41 += sprite.Position.X - m11 * sprite.Center.X - m21 * sprite.Center.Y ;
			wvp.M42 += sprite.Position.Y - m12 * sprite.Center.X - m22 * sprite.Center.Y ;
		}

		//  Projection matrix

		float w = 2.0f / fbuffer.Width ;
		float h = -2.0f / fbuffer.Height ;
		wvp.M11 = w * wvp.M11 ;	wvp.M12 = h * wvp.M12 ;
		wvp.M21 = w * wvp.M21 ;	wvp.M22 = h * wvp.M22 ;
		wvp.M41 = w * wvp.M41 - 1.0f ;
		wvp.M42 = h * wvp.M42 + 1.0f ;
	}
	public static void CosSin( float x, out Vector2 result )
	{
		#if !ENABLE_SIN_TABLE
		result.X = (float)Math.Cos( x ) ;
		result.Y = (float)Math.Sin( x ) ;
		#else // ENABLE_SIN_TABLE
		int phase = (int)( x * ( 2048.0f / FMath.PI ) ) ;
		result.X = sinTable[ ( phase + 1024 ) & 4095 ] ;
		result.Y = sinTable[ phase & 4095 ] ;
		#endif // ENABLE_SIN_TABLE
	}

	//  Subroutines

	internal void UpdateSortKey()
	{
		long newKey = ( (long)zIndex << 32 ) | ( material.MaterialID << 8 ) | DrawTypeID ;
		if ( batch != null ) batch.RemoveFromSortedList( this ) ;
		sortKey = newKey ;
		if ( batch != null ) batch.AddToSortedList( this ) ;
	}
	internal void UpdateVertex( out SpriteBatch.Vertex vertex )
	{
		vertex.Position = Position ;
		vertex.Direction = Direction ;
		vertex.Size.X = Size.X * Scale.X ;
		vertex.Size.Y = Size.Y * Scale.Y ;
		vertex.Center = Center ;
		vertex.UVOffset = UVOffset ;
		vertex.UVSize = UVSize ;
		vertex.Color = Color ;
	}
	internal void UpdatePosition( ref SpriteBatch.Vertex vertex )
	{
		vertex.Position = Position ;
		vertex.Direction = Direction ;
		vertex.Size.X = Size.X * Scale.X ;
		vertex.Size.Y = Size.Y * Scale.Y ;
		vertex.Center = Center ;
	}

	//  Params

	public virtual SpriteBatch Batch {
		get { return batch ; }
		set {
			if ( value != batch && value != null ) value.AddSprite( this ) ;
			if ( value != batch && value == null ) batch.RemoveSprite( this ) ;
		}
	}
	public virtual SpriteMaterial Material {
		get { return material ; }
		set { material = value ; UpdateSortKey() ; }
	}
	public virtual int ZIndex {
		get { return zIndex ; }
		set { zIndex = value ; UpdateSortKey() ; }
	}
	public Vector2 Position ;
	public Vector2 Velocity ;
	public float Direction ;
	public float Rotation ;
	public Vector2 Size ;
	public Vector2 Scale ;
	public Vector2 Center ;
	public Vector2 UVOffset ;
	public Vector2 UVSize ;
	public Rgba Color ;

	//  Internal params

	internal SpriteBatch batch ;
	internal SpriteMaterial material ;
	internal Sprite sortPrev, sortNext ;
	internal int vertexID ;
	internal int zIndex ;
	internal long sortKey ;
	#if ENABLE_SIN_TABLE
	internal static float[] sinTable ;
	#endif // ENABLE_SIN_TABLE
}


} //  end ns Sample
