/* PlayStation(R)Mobile SDK 2.00.00
 * Copyright (C) 2014 Sony Computer Entertainment Inc.
 * All Rights Reserved.
 */
using System;
using System.IO;
using System.Threading;
using System.Diagnostics;
using Sce.PlayStation.Core;
using Sce.PlayStation.Core.Graphics;
using Sce.PlayStation.Core.Environment;
using Sce.PlayStation.Core.Input;

namespace Sample
{

/**
 * TextureSample
 */
class TextureSample
{
    private static GraphicsContext graphics;
    private static Stopwatch stopwatch;
    private static ShaderProgram program;
    private static VertexBuffer vbuffer;

    private static Texture2D[] textures;
    private static string[] filenames;
    private static string[] texinfos;
    private static int texindex;

    static bool loop = true;

    static void Main(string[] args)
    {
        Init();

        while (loop) {
            SystemEvents.CheckEvents();
            Update();
            Render();
        }
        Term();
    }

    static bool Init()
    {
        graphics = new GraphicsContext();
        stopwatch = new Stopwatch();
        stopwatch.Start();

        SampleDraw.Init(graphics);

        program = new ShaderProgram("/Application/shaders/Texture.cgx");
        vbuffer = new VertexBuffer(4, VertexFormat.Float3, VertexFormat.Float2);

        program.SetUniformBinding(0, "WorldViewProj");
        program.SetAttributeBinding(0, "a_Position");
        program.SetAttributeBinding(1, "a_TexCoord");

        float[] vertices = {
            0.0f, 0.0f, 0.0f,  0.0f, 0.0f,
            0.0f, 1.0f, 0.0f,  0.0f, 1.0f,
            1.0f, 0.0f, 0.0f,  1.0f, 0.0f,
            1.0f, 1.0f, 0.0f,  1.0f, 1.0f,
        };
        vbuffer.SetVertices(vertices);

        int[] bpps = {0, 32, 64, 16, 16, 16, 16, 32, 8, 16, 8, 16, 16, 32, 32, 32, 4, 8, 8, 8, 8};
        filenames = Directory.GetFiles("/Application/textures");
        textures = new Texture2D[filenames.Length];
        texinfos = new string[filenames.Length];
        for (int i = 0; i < filenames.Length; i ++) {
            filenames[i] = Path.GetFileName(filenames[i]);

            string filepath = "/Application/textures/" + filenames[i];
            string filepath2 = "/Application/textures2/" + filenames[i];
            if (!File.Exists(filepath2)) filepath2 = filepath;

            var t1 = stopwatch.ElapsedTicks;
            textures[i] = new Texture2D(filepath, false);
            var t2 = stopwatch.ElapsedTicks;
            textures[i] = new Texture2D(filepath2, false);
            var t3 = stopwatch.ElapsedTicks;

            int bpp = bpps[(int)textures[i].Format];
            long size = (new FileInfo(filepath).Length + 1023) / 1024;
            int size2 = ((textures[i].Width * bpp + 7) / 8 * textures[i].Height + 1023) / 1024;
            float time = (float)(t2 - t1) * 1000.0f / Stopwatch.Frequency;
            float time2 = (float)(t3 - t2) * 1000.0f / Stopwatch.Frequency;

            string format = "{0} KiB  {1} KiB  {2:F1} ms  {3:F1} ms";
            texinfos[i] = string.Format(format, size, size2, time, time2);
        }
        return true;
    }

    static void Term()
    {
        SampleDraw.Term();
        program.Dispose();
        vbuffer.Dispose();
        for (int i = 0; i < textures.Length; i ++) textures[i].Dispose();
        graphics.Dispose();
    }

    static bool Update()
    {
        SampleDraw.Update();

        var padData = GamePad.GetData(0);
        var press = padData.ButtonsDown;
        if ((GamePadButtons.Up & press) != 0) texindex -= 1;
        if ((GamePadButtons.Down & press) != 0) texindex += 1;
        if (texindex < 0) texindex = 0;
        if (texindex > textures.Length - 1) texindex = textures.Length - 1;
        return true;
    }

    static bool Render()
    {
        int sw = graphics.Screen.Width;
        int sh = graphics.Screen.Height;
        int tw = textures[texindex].Width;
        int th = textures[texindex].Height;
        Matrix4 proj = Matrix4.Ortho(0, sw, sh, 0, 0.0f, 32768.0f);
        Matrix4 view = Matrix4.Identity;
        Matrix4 world = Matrix4.Translation(sw - 24 - tw, sh - 24 - th, 0) * Matrix4.Scale(tw, th, 0);

        Matrix4 worldViewProj = proj * view * world;
        program.SetUniformValue(0, ref worldViewProj);

        graphics.SetViewport(0, 0, graphics.Screen.Width, graphics.Screen.Height);
        graphics.SetClearColor(0.0f, 0.5f, 1.0f, 0.0f);
        graphics.Clear();

        graphics.SetShaderProgram(program);
        graphics.SetVertexBuffer(0, vbuffer);
        graphics.SetTexture(0, textures[texindex]);
        graphics.DrawArrays(DrawMode.TriangleStrip, 0, 4);

        int y = graphics.Screen.Height - 24 - 28 * filenames.Length;
        SampleDraw.DrawText("FileName", 0xffffffff, 24, y - 36);
        SampleDraw.DrawText("FileSize MemSize LoadTime LoadTime(Plain)", 0xffffffff, 224, y - 36);
        for (int i = 0; i < filenames.Length; i ++) {
            uint argb = (texindex == i) ? 0xff0000ff : 0xffffffff ;
            SampleDraw.DrawText(filenames[i], argb, 24, y + 28 * i);
            SampleDraw.DrawText(texinfos[i], argb, 224, y + 28 * i);
        }

        SampleDraw.DrawText("Texture Sample", 0xffffffff, 0, 0);

        graphics.SwapBuffers();
        return true;
    }
}

} // end ns Sample
