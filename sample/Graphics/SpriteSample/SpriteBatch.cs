/* PlayStation(R)Mobile SDK 2.00.00
 * Copyright (C) 2014 Sony Computer Entertainment Inc.
 * All Rights Reserved.
 */

#define RESIZE_VERTEX_BUFFER
#define ENABLE_SIN_TABLE

using System ;
using System.Threading ;
using System.Diagnostics ;
using System.Runtime.InteropServices;
using Sce.PlayStation.Core ;
using Sce.PlayStation.Core.Graphics ;

namespace Sample
{

//----------------------------------------------------------------
//  Sprite Batch
//----------------------------------------------------------------

public class SpriteBatch : Sprite, IDisposable
{
	public SpriteBatch( int maxSpriteCount, int zIndex = 0 ) : base( null, zIndex )
	{
		if ( maxSpriteCount > 10000 ) {
			throw new ArgumentOutOfRangeException( "maxSpriteCount > 10000" ) ;
		}
		int maxVertexCount = maxSpriteCount * 4 ;
		int maxIndexCount = maxSpriteCount * 6 ;

		#if !RESIZE_VERTEX_BUFFER
		vertexBuffer = new VertexBuffer( maxVertexCount, maxIndexCount, vertexFormats ) ;
		#endif // RESIZE_VERTEX_BUFFER
		vertexData = new Vertex[ maxVertexCount ] ;
		indexData = new ushort[ maxIndexCount ] ;

		spriteList = new Sprite[ maxSpriteCount ] ;
		sortedList = new Sprite[ maxSpriteCount ] ;
		spriteCapacity = maxSpriteCount ;

		Center = Vector2.Zero ;		// Top-left anchor
	}
	public void Dispose()
	{
		Dispose( true ) ;
	}
	protected virtual void Dispose( bool disposing )
	{
		if ( disposing ) {
			if ( vertexBuffer != null ) vertexBuffer.Dispose() ;
			vertexBuffer = null ;
			vertexData = null ;
			indexData = null ;
			spriteList = null ;
			sortedList = null ;
		}
	}

	//  Functions

	public override void Draw( GraphicsContext graphics )
	{
		if ( spriteCount == 0 ) return ;
		#if RESIZE_VERTEX_BUFFER
		ResizeBuffer() ;
		#endif // RESIZE_VERTEX_BUFFER
		SortIndexData() ;
		CopyIndexData() ;
		CopyVertexData() ;
		DrawSprites( graphics ) ;
	}
	public void AddSprite( Sprite sprite )
	{
		if ( sprite.batch != null ) sprite.batch.RemoveSprite( sprite ) ;
		if ( spriteCount >= spriteCapacity ) throw new ArgumentOutOfRangeException() ;
		AddToSpriteList( sprite ) ;
		AddToSortedList( sprite ) ;
		vertexCount += 4 ;
		indexCount += 6 ;
		sprite.batch = this ;		// Suppress recursion
		sprite.Batch = this ;		// Virtual property
		if ( sprite.DrawTypeID == 0 ) sprite.UpdateAll() ;
	}
	public void RemoveSprite( Sprite sprite )
	{
		if ( sprite.batch != this ) throw new InvalidOperationException() ;
		RemoveFromSpriteList( sprite ) ;
		RemoveFromSortedList( sprite ) ;
		vertexCount -= 4 ;
		indexCount -= 6 ;
		sprite.batch = null ;		// Suppress recursion
		sprite.Batch = null ;		// Virtual property
	}

	//  Params

	public int SpriteCapacity {
		get { return spriteCapacity ; }
	}
	public int SpriteCount {
		get { return spriteCount ; }
	}

	//  Overrides

	public override byte DrawTypeID {
		get { return 1 ; }
	}

	//  Subroutines

	#if RESIZE_VERTEX_BUFFER
	internal void ResizeBuffer()
	{
		int oldCapacity = bufferCapacity ;
		if ( spriteCount > bufferCapacity ) {
			int threshold = (int)( bufferCapacity * bufferResizeFactor ) ;
			bufferCapacity = spriteCount + bufferResizeMargin ;
			if ( bufferCapacity < threshold ) bufferCapacity = threshold ;
			if ( bufferCapacity > spriteCapacity ) bufferCapacity = spriteCapacity ;
			bufferReducePending = 0 ;
			bufferReduceTarget = 0 ;
		} else {
			if ( spriteCount > bufferReduceTarget ) bufferReduceTarget = spriteCount ;
			if ( ++ bufferReducePending >= bufferReduceWait ) {
				int threshold = (int)( bufferCapacity / bufferResizeFactor ) ;
				if ( bufferReduceTarget + bufferResizeMargin * 2 <= threshold ) {
					bufferCapacity = bufferReduceTarget + bufferResizeMargin ;
				}
				bufferReducePending = 0 ;
				bufferReduceTarget = 0 ;
			}
		}
		if ( bufferCapacity != oldCapacity ) {
			if ( vertexBuffer != null ) vertexBuffer.Dispose() ;
			vertexBuffer = new VertexBuffer( bufferCapacity * 4, bufferCapacity * 6, vertexFormats ) ;
			needCopyVertexData = true ;
			needCopyIndexData = true ;
		}
	}
	#endif // RESIZE_VERTEX_BUFFER
	internal void CopyVertexData()
	{
		if ( !needCopyVertexData ) return ;
		needCopyVertexData = false ;
		vertexBuffer.SetVertices( vertexData, 0, 0, vertexCount ) ;
	}
	internal void CopyIndexData()
	{
		if ( !needCopyIndexData ) return ;
		needCopyIndexData = false ;
		vertexBuffer.SetIndices( indexData, 0, 0, indexCount ) ;
	}
	internal void SortIndexData()
	{
		if ( !needSortIndexData ) return ;
		needSortIndexData = false ;
		needCopyIndexData = true ;

		int indexID = 0 ;
		var sprite = sortedList[ 0 ] ;
		for ( int i = 0 ; i < spriteCount ; i ++ ) {
			if ( sprite.indexID != indexID ) {
				sprite.indexID = indexID ;
				indexData[ indexID + 0 ] = (ushort)( sprite.vertexID + 0 ) ;
				indexData[ indexID + 1 ] = (ushort)( sprite.vertexID + 1 ) ;
				indexData[ indexID + 2 ] = (ushort)( sprite.vertexID + 2 ) ;
				indexData[ indexID + 3 ] = (ushort)( sprite.vertexID + 2 ) ;
				indexData[ indexID + 4 ] = (ushort)( sprite.vertexID + 1 ) ;
				indexData[ indexID + 5 ] = (ushort)( sprite.vertexID + 3 ) ;
			}
			sprite = sprite.sortNext ;
			indexID += 6 ;
		}
	}
	internal void DrawSprites( GraphicsContext graphics )
	{
		Matrix4 wvp ; Vector4 col ;
		GetTransform( graphics.GetFrameBuffer(), out wvp, out col ) ;

		graphics.Enable( EnableMode.Blend ) ;
		graphics.SetVertexBuffer( 0, vertexBuffer ) ;

		Sprite start = sortedList[ 0 ] ;
		for ( int i = 1 ; i <= sortedCount ; i ++ ) {
			var next = sortedList[ i % sortedCount ] ;
			if ( (uint)start.sortKey != (uint)next.sortKey || i == sortedCount ) {
				if ( start.DrawTypeID != 0 ) {
					//  Custom drawing
					do { start.Draw( graphics ) ; start = start.sortNext ; } while ( start != next ) ;
					graphics.Enable( EnableMode.Blend ) ;
					graphics.SetVertexBuffer( 0, vertexBuffer ) ;
					continue ;
				}
				var material = start.material ;
				int nextID = ( i < sortedCount ) ? next.indexID : indexCount ;
				material.ShaderProgram.SetUniformValue( 0, ref wvp ) ;
				material.ShaderProgram.SetUniformValue( 1, ref col ) ;
				graphics.SetShaderProgram( material.ShaderProgram ) ;
				graphics.SetBlendFunc( material.BlendFunc ) ;
				graphics.SetTexture( 0, material.Texture ) ;
				graphics.DrawArrays( DrawMode.Triangles, start.indexID, nextID - start.indexID ) ;
				start = next ;
			}
		}
	}

	//  Subroutines

	internal void AddToSpriteList( Sprite sprite )
	{
		sprite.spriteID = spriteCount ;
		sprite.vertexID = vertexCount ;
		spriteList[ spriteCount ++ ] = sprite ;
		needCopyVertexData = true ;
	}
	internal void RemoveFromSpriteList( Sprite sprite )
	{
		var tail = spriteList[ -- spriteCount ] ;
		spriteList[ spriteCount ] = null ;
		if ( sprite != tail ) {
			spriteList[ sprite.spriteID ] = tail ;
			for ( int i = 0 ; i < 4 ; i ++ ) {
				vertexData[ sprite.vertexID + i ] = vertexData[ tail.vertexID + i ] ;
			}
			tail.spriteID = sprite.spriteID ;
			tail.vertexID = sprite.vertexID ;
			tail.indexID = -1 ;
			needSortIndexData = true ;
		}
		needCopyVertexData = true ;
	}
	internal void AddToSortedList( Sprite sprite )
	{
		int index = FindSortedList( sprite ) ;
		if ( index < 0 || sortedList[ index ].sortKey != sprite.sortKey ) {
			int count = ( sortedCount ++ ) - ( ++ index ) ;
			if ( count > 0 ) Array.Copy( sortedList, index, sortedList, index + 1, count ) ;
			sortedList[ index ] = sprite ;
			sprite.sortPrev = sprite ;
			sprite.sortNext = sprite ;
		}
		var next = sortedList[ ( index + 1 ) % sortedCount ] ;
		var prev = next.sortPrev ;
		next.sortPrev = sprite ;
		prev.sortNext = sprite ;
		sprite.sortPrev = prev ;
		sprite.sortNext = next ;
		sprite.indexID = -1 ;
		needSortIndexData = true ;
	}
	internal void RemoveFromSortedList( Sprite sprite )
	{
		int index = FindSortedList( sprite ) ;
		if ( sortedList[ index ] == sprite ) {
			sortedList[ index ] = sprite.sortNext ;
			if ( sprite.sortNext == sortedList[ ( index + 1 ) % sortedCount ] ) {
				int count = ( -- sortedCount ) - index ;
				if ( count > 0 ) Array.Copy( sortedList, index + 1, sortedList, index, count ) ;
				sortedList[ sortedCount ] = null ;
			}
		}
		sprite.sortPrev.sortNext = sprite.sortNext ;
		sprite.sortNext.sortPrev = sprite.sortPrev ;
		needSortIndexData = true ;
	}
	internal int FindSortedList( Sprite sprite )
	{
		int lower = -1 ;
		int upper = sortedCount ;
		while ( lower + 1 < upper ) {
			int middle = ( lower + upper ) / 2 ;
			if ( sortedList[ middle ].sortKey > sprite.sortKey ) {
				upper = middle ;
			} else {
				lower = middle ;
			}
		}
		return lower ;
	}

	//  Internal params

	internal VertexBuffer vertexBuffer ;
	internal Vertex[] vertexData ;
	internal ushort[] indexData ;
	internal int vertexCount ;
	internal int indexCount ;
	internal bool needCopyVertexData ;
	internal bool needCopyIndexData ;
	internal bool needSortIndexData ;

	internal Sprite[] spriteList ;
	internal Sprite[] sortedList ;
	internal int spriteCapacity ;
	internal int spriteCount ;
	internal int sortedCount ;

	#if RESIZE_VERTEX_BUFFER
	const float bufferResizeFactor = 1.5f ;
	const int bufferResizeMargin = 256 ;
	const int bufferReduceWait = 30 ;
	internal int bufferReducePending ;
	internal int bufferReduceTarget ;
	internal int bufferCapacity ;
	#endif // RESIZE_VERTEX_BUFFER

	//  Vertex format

	internal static VertexFormat[] vertexFormats = {
		VertexFormat.Float2,	// Position
		VertexFormat.Float2,	// TexCoord
		VertexFormat.UByte4N	// Color
	} ;
	[StructLayout(LayoutKind.Explicit,Size=20)]
	internal struct Vertex {
		[FieldOffset(0)]
		public Vector2 Position ;
		[FieldOffset(8)]
		public Vector2 TexCoord ;
		[FieldOffset(16)]
		public Rgba Color ;
	}
}

//----------------------------------------------------------------
//  Sprite Material
//----------------------------------------------------------------

public class SpriteMaterial : IDisposable
{
	public SpriteMaterial( Texture2D texture = null )
	{
		MaterialID = ( nextMaterialID ++ ) & 0x00ffffff ;
		ShaderProgram = DefaultShaderProgram ;
		BlendFunc = DefaultBlendFunc ;
		Texture = texture ?? DefaultTexture ;
	}
	public void Dispose()
	{
		Dispose( true ) ;
	}
	protected virtual void Dispose( bool disposing )
	{
		if ( disposing ) {
			if ( ShaderProgram != null && ShaderProgram != defaultShaderProgram ) ShaderProgram.Dispose() ;
			if ( Texture != null && Texture != defaultTexture ) Texture.Dispose() ;
			ShaderProgram = null ;
			Texture = null ;
		}
	}

	//  Params

	public readonly uint MaterialID ;
	public ShaderProgram ShaderProgram ;
	public BlendFunc BlendFunc ;
	public Texture2D Texture ;

	//  Default values

	public static SpriteMaterial DefaultMaterial {
		get {
			if ( defaultMaterial == null ) {
				defaultMaterial = new SpriteMaterial() ;
			}
			return defaultMaterial ;
		}
	}
	public static ShaderProgram DefaultShaderProgram {
		get {
			if ( defaultShaderProgram == null ) {
				var filename = "/Application/shaders/SpriteBatch.cgx" ;
				defaultShaderProgram = new ShaderProgram( filename ) ;
			}
			return defaultShaderProgram ;
		}
	}
	public static BlendFunc DefaultBlendFunc {
		get {
			return new BlendFunc( BlendFuncMode.Add,
				BlendFuncFactor.SrcAlpha, BlendFuncFactor.OneMinusSrcAlpha ) ;
		}
	}
	public static Texture2D DefaultTexture {
		get {
			if ( defaultTexture == null ) {
				defaultTexture = new Texture2D( 1, 1, false, PixelFormat.Rgba ) ;
				Rgba[] pixels = { new Rgba( 255, 255, 255, 255 ) } ;
				defaultTexture.SetPixels( 0, pixels ) ;
			}
			return defaultTexture ;
		}
	}

	internal static uint nextMaterialID ;
	internal static SpriteMaterial defaultMaterial ;
	internal static ShaderProgram defaultShaderProgram ;
	internal static Texture2D defaultTexture ;
}

//----------------------------------------------------------------
//  Sprite
//----------------------------------------------------------------

public class Sprite
{
	public Sprite( SpriteMaterial material = null, int zIndex = 0 )
	{
		this.material = material ?? SpriteMaterial.DefaultMaterial ;
		this.zIndex = zIndex ;
		UpdateSortKey() ;

		Scale = new Vector2( 1.0f ) ;
		Center = new Vector2( 0.5f ) ;
		UVSize = new Vector2( 1.0f ) ;
		Color = new Rgba( 255, 255, 255, 255 ) ;

		#if ENABLE_SIN_TABLE
		if ( sinTable == null ) {
			sinTable = new float[ 4096 ] ;
			for ( int i = 0 ; i < 4096 ; i ++ ) {
				sinTable[ i ] = FMath.Sin( i * ( FMath.PI / 2048.0f ) ) ;
			}
		}
		#endif // ENABLE_SIN_TABLE
	}

	//  Vertex update

	public void UpdateAll()
	{
		UpdatePosition() ;
		UpdateTexCoord() ;
		UpdateColor() ;
	}
	public void UpdatePosition()
	{
		if ( batch == null ) return ;
		var vertexData = batch.vertexData ;
		#if !ENABLE_SIN_TABLE
		float c = (float)Math.Cos( Direction ) ;
		float s = (float)Math.Sin( Direction ) ;
		#else // ENABLE_SIN_TABLE
		int phase = (int)( Direction * ( 2048.0f / FMath.PI ) ) ;
		float c = sinTable[ ( phase + 1024 ) & 4095 ] ;
		float s = sinTable[ phase & 4095 ] ;
		#endif // ENABLE_SIN_TABLE
		float ux = c * Size.X * Scale.X ;	float vx = -s * Size.Y * Scale.Y ;
		float uy = s * Size.X * Scale.X ;	float vy = c * Size.Y * Scale.Y ;
		float x = Position.X - ux * Center.X - vx * Center.Y ;
		float y = Position.Y - uy * Center.X - vy * Center.Y ;
		vertexData[ vertexID + 0 ].Position.X = x ;
		vertexData[ vertexID + 0 ].Position.Y = y ;
		vertexData[ vertexID + 1 ].Position.X = x + vx ;
		vertexData[ vertexID + 1 ].Position.Y = y + vy ;
		vertexData[ vertexID + 2 ].Position.X = x + ux ;
		vertexData[ vertexID + 2 ].Position.Y = y + uy ;
		vertexData[ vertexID + 3 ].Position.X = x + ux + vx ;
		vertexData[ vertexID + 3 ].Position.Y = y + uy + vy ;
		batch.needCopyVertexData = true ;
	}
	public void UpdateTexCoord()
	{
		if ( batch == null ) return ;
		var vertexData = batch.vertexData ;
		vertexData[ vertexID + 0 ].TexCoord.X = UVOffset.X ;
		vertexData[ vertexID + 0 ].TexCoord.Y = UVOffset.Y ;
		vertexData[ vertexID + 1 ].TexCoord.X = UVOffset.X ;
		vertexData[ vertexID + 1 ].TexCoord.Y = UVOffset.Y + UVSize.Y ;
		vertexData[ vertexID + 2 ].TexCoord.X = UVOffset.X + UVSize.X ;
		vertexData[ vertexID + 2 ].TexCoord.Y = UVOffset.Y ;
		vertexData[ vertexID + 3 ].TexCoord.X = UVOffset.X + UVSize.X ;
		vertexData[ vertexID + 3 ].TexCoord.Y = UVOffset.Y + UVSize.Y ;
		batch.needCopyVertexData = true ;
	}
	public void UpdateColor()
	{
		if ( batch == null ) return ;
		var vertexData = batch.vertexData ;
		for ( int i = 0 ; i < 4 ; i ++ ) {
			vertexData[ vertexID + i ].Color = Color ;
		}
		batch.needCopyVertexData = true ;
	}

	//  Custom drawing

	public virtual void Draw( GraphicsContext graphics )
	{
		throw new NotSupportedException() ;		// Custom drawing function
	}
	public virtual byte DrawTypeID {
		get { return 0 ; }						// Custom drawing type-id
	}
	public void GetTransform( FrameBuffer fbuffer, out Matrix4 wvp, out Vector4 col )
	{
		var sprite = this ;

		//  World matrix

		Vector2 cs ;
		CosSin( sprite.Direction, out cs ) ;
		col = sprite.Color.ToVector4() ;
		wvp = Matrix4.Identity ;
		wvp.M11 = cs.X * sprite.Scale.X ;	wvp.M12 = cs.Y * sprite.Scale.X ;
		wvp.M21 = -cs.Y * sprite.Scale.Y ;	wvp.M22 = cs.X * sprite.Scale.Y ;
		wvp.M41 = sprite.Position.X - wvp.M11 * sprite.Center.X - wvp.M21 * sprite.Center.Y ;
		wvp.M42 = sprite.Position.Y - wvp.M12 * sprite.Center.X - wvp.M22 * sprite.Center.Y ;

		//  Parent matrix

		while ( ( sprite = sprite.batch ) != null ) {
			CosSin( sprite.Direction, out cs ) ;
			col *= sprite.Color.ToVector4() ;
			float m11 = cs.X * sprite.Scale.X ;	float m12 = cs.Y * sprite.Scale.X ;
			float m21 = -cs.Y * sprite.Scale.Y ;	float m22 = cs.X * sprite.Scale.Y ;
			float M11 = wvp.M11 ;				float M12 = wvp.M12 ;
			float M21 = wvp.M21 ;				float M22 = wvp.M22 ;
			float M41 = wvp.M41 ;				float M42 = wvp.M42 ;
			wvp.M11 = m11 * M11 + m21 * M12 ;	wvp.M12 = m12 * M11 + m22 * M12 ;
			wvp.M21 = m11 * M21 + m21 * M22 ;	wvp.M22 = m12 * M21 + m22 * M22 ;
			wvp.M41 = m11 * M41 + m21 * M42 ;	wvp.M42 = m12 * M41 + m22 * M42 ;
			wvp.M41 += sprite.Position.X - m11 * sprite.Center.X - m21 * sprite.Center.Y ;
			wvp.M42 += sprite.Position.Y - m12 * sprite.Center.X - m22 * sprite.Center.Y ;
		}

		//  Projection matrix

		float w = 2.0f / fbuffer.Width ;
		float h = -2.0f / fbuffer.Height ;
		wvp.M11 = w * wvp.M11 ;	wvp.M12 = h * wvp.M12 ;
		wvp.M21 = w * wvp.M21 ;	wvp.M22 = h * wvp.M22 ;
		wvp.M41 = w * wvp.M41 - 1.0f ;
		wvp.M42 = h * wvp.M42 + 1.0f ;
	}
	public static void CosSin( float x, out Vector2 result )
	{
		#if !ENABLE_SIN_TABLE
		result.X = (float)Math.Cos( x ) ;
		result.Y = (float)Math.Sin( x ) ;
		#else // ENABLE_SIN_TABLE
		int phase = (int)( x * ( 2048.0f / FMath.PI ) ) ;
		result.X = sinTable[ ( phase + 1024 ) & 4095 ] ;
		result.Y = sinTable[ phase & 4095 ] ;
		#endif // ENABLE_SIN_TABLE
	}

	//  Subroutines

	internal void UpdateSortKey()
	{
		long newKey = ( (long)zIndex << 32 ) | ( material.MaterialID << 8 ) | DrawTypeID ;
		if ( batch != null ) batch.RemoveFromSortedList( this ) ;
		sortKey = newKey ;
		if ( batch != null ) batch.AddToSortedList( this ) ;
	}

	//  Params

	public virtual SpriteBatch Batch {
		get { return batch ; }
		set {
			if ( value != batch && value != null ) value.AddSprite( this ) ;
			if ( value != batch && value == null ) batch.RemoveSprite( this ) ;
		}
	}
	public virtual SpriteMaterial Material {
		get { return material ; }
		set { material = value ; UpdateSortKey() ; }
	}
	public virtual int ZIndex {
		get { return zIndex ; }
		set { zIndex = value ; UpdateSortKey() ; }
	}
	public Vector2 Position ;
	public Vector2 Velocity ;
	public float Direction ;
	public float Rotation ;
	public Vector2 Size ;
	public Vector2 Scale ;
	public Vector2 Center ;
	public Vector2 UVOffset ;
	public Vector2 UVSize ;
	public Rgba Color ;

	//  Internal params

	internal SpriteBatch batch ;
	internal SpriteMaterial material ;
	internal Sprite sortPrev, sortNext ;
	internal int spriteID ;
	internal int vertexID ;
	internal int indexID ;
	internal int zIndex ;
	internal long sortKey ;
	#if ENABLE_SIN_TABLE
	internal static float[] sinTable ;
	#endif // ENABLE_SIN_TABLE
}


} //  end ns Sample
