/* PlayStation(R)Mobile SDK 2.00.00
 * Copyright (C) 2014 Sony Computer Entertainment Inc.
 * All Rights Reserved.
 */

using System;

namespace AppRpg {

///***************************************************************************
///***************************************************************************
public static class ShapeObj05Data {

    public static float[] Positions = {
        -0.496024f, -0.007363f, -1.115385f,
        -0.496024f, 0.633511f, -1.115385f,
        0.496024f, 0.633511f, -1.115385f,
        -0.496024f, -0.007363f, -1.115385f,
        0.496024f, 0.633511f, -1.115385f,
        0.496024f, -0.007363f, -1.115385f,
        -0.496024f, -0.007363f, 1.115385f,
        0.496024f, -0.007363f, 1.115385f,
        0.496024f, 0.946773f, 1.115385f,
        -0.496024f, -0.007363f, 1.115385f,
        0.496024f, 0.946773f, 1.115385f,
        -0.496024f, 0.946773f, 1.115385f,
        -0.496024f, -0.007363f, -1.115385f,
        -0.496024f, -0.007363f, 1.115385f,
        -0.496024f, 0.946773f, 1.115385f,
        -0.496024f, -0.007363f, -1.115385f,
        -0.496024f, 0.946773f, 1.115385f,
        -0.496024f, 0.633511f, -1.115385f,
        0.496024f, -0.007363f, -1.115385f,
        0.496024f, 0.633511f, -1.115385f,
        0.496024f, 0.946773f, 1.115385f,
        0.496024f, -0.007363f, -1.115385f,
        0.496024f, 0.946773f, 1.115385f,
        0.496024f, -0.007363f, 1.115385f,
        -0.496024f, 0.633511f, -1.115385f,
        -0.496024f, 0.946773f, 1.115385f,
        0.496024f, 0.946773f, 1.115385f,
        -0.496024f, 0.633511f, -1.115385f,
        0.496024f, 0.946773f, 1.115385f,
        0.496024f, 0.633511f, -1.115385f,
    };
}
} // namespace
