/* PlayStation(R)Mobile SDK 2.00.00
 * Copyright (C) 2014 Sony Computer Entertainment Inc.
 * All Rights Reserved.
 */

using System;

namespace AppRpg {

///***************************************************************************
///***************************************************************************
public static class ShapeObj04Data {

    public static float[] Positions = {
        -0.488813f, 0.000000f, -0.045135f,
        -0.488813f, 1.567729f, -0.045135f,
        0.488813f, 1.567729f, -0.045135f,
        -0.488813f, 0.000000f, -0.045135f,
        0.488813f, 1.567729f, -0.045135f,
        0.488813f, 0.000000f, -0.045135f,
        -0.488813f, 0.000000f, 0.129838f,
        0.488813f, 0.000000f, 0.129838f,
        0.488813f, 1.567729f, 0.129838f,
        -0.488813f, 0.000000f, 0.129838f,
        0.488813f, 1.567729f, 0.129838f,
        -0.488813f, 1.567729f, 0.129838f,
        -0.488813f, 0.000000f, -0.045135f,
        -0.488813f, 0.000000f, 0.129838f,
        -0.488813f, 1.567729f, 0.129838f,
        -0.488813f, 0.000000f, -0.045135f,
        -0.488813f, 1.567729f, 0.129838f,
        -0.488813f, 1.567729f, -0.045135f,
        0.488813f, 0.000000f, -0.045135f,
        0.488813f, 1.567729f, -0.045135f,
        0.488813f, 1.567729f, 0.129838f,
        0.488813f, 0.000000f, -0.045135f,
        0.488813f, 1.567729f, 0.129838f,
        0.488813f, 0.000000f, 0.129838f,
    };
}
} // namespace
