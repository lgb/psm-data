/* PlayStation(R)Mobile SDK 2.00.00
 * Copyright (C) 2014 Sony Computer Entertainment Inc.
 * All Rights Reserved.
 */
using System;
using System.Collections.Generic;
using Sce.PlayStation.Core;
using DemoGame;

namespace ShootingDemo
{

/**
 * NoneBulletModelクラス
 */
public class NoneBulletModel : MonoModel
{
    /// コンストラクタ
    public NoneBulletModel() : base()
    {
        modelInfo = new MonoModelInfo(48, CollisionLevel.Bullet);
    }

}

} // ShootingDemo
