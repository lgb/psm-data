/* PlayStation(R)Mobile SDK 2.00.00
 * Copyright (C) 2014 Sony Computer Entertainment Inc.
 * All Rights Reserved.
 */
using System;

namespace ShootingDemo
{

/**
 * BossBulletHandle
 */
public class BossBulletHandle : ScriptUnitHandle
{
    /// コンストラクタ
    public BossBulletHandle()
    {
    }

    /// 開始
    public override bool Start(MonoManager monoManager, Mono mono)
    {
        MoveBasic(199, 0, 0, 8, 0, 0, 0);

        return true;
    }
}

} // ShootingDemo
