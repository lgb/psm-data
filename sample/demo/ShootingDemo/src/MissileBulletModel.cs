/* PlayStation(R)Mobile SDK 2.00.00
 * Copyright (C) 2014 Sony Computer Entertainment Inc.
 * All Rights Reserved.
 */
using System;
using System.Collections.Generic;
using Sce.PlayStation.Core;

namespace ShootingDemo
{

/**
 * MissileBulletModelクラス
 */
public class MissileBulletModel : MonoModel
{
    /// コンストラクタ
    public MissileBulletModel() : base()
    {
        modelInfo = new MonoModelInfo(8, CollisionLevel.Bullet);

        action = new ModelAction();
        action.Add("Default", new List<ModelAnim>() {createAnim1("MissileBullet")});

        SetAction("Default");
    }
}

} // ShootingDemo
