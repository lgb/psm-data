/* PlayStation(R)Mobile SDK 2.00.00
 * Copyright (C) 2014 Sony Computer Entertainment Inc.
 * All Rights Reserved.
 */
using System;

namespace ShootingDemo
{

/**
 * ShootingDemo
 */
public static class ShootingDemo
{
    private static GameMain gameMain;

    /// エントリーポイント
    public static void Main(string[] args)
    {
        gameMain = new GameMain();
        gameMain.SetUpperLimitFps(GameData.TargetFps);
        gameMain.Run(args);
    }
}

} // ShootingDemo
