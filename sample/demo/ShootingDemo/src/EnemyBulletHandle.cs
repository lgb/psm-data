/* PlayStation(R)Mobile SDK 2.00.00
 * Copyright (C) 2014 Sony Computer Entertainment Inc.
 * All Rights Reserved.
 */
using System;

namespace ShootingDemo
{

/**
 * EnemyBulletHandle
 */
public class EnemyBulletHandle : ScriptUnitHandle
{
    /// コンストラクタ
    public EnemyBulletHandle()
    {
    }

    /// 開始
    public override bool Start(MonoManager monoManager, Mono mono)
    {
        MoveBasic(199, 0, 0, 50, 0, 0, 0);

        return true;
    }
}

} // ShootingDemo
