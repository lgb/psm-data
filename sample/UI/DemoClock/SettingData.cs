﻿/* PlayStation(R)Mobile SDK 2.00.00
 * Copyright (C) 2014 Sony Computer Entertainment Inc.
 * All Rights Reserved.
 */


using System;
namespace DemoClock
{
    public enum ClockType
    {
        Analog,
        Flip
    };
    
    public class SettingData
    {
        public SettingData ()
        {
            ClockType = ClockType.Analog;
        }

        public ClockType ClockType;
    }
}

