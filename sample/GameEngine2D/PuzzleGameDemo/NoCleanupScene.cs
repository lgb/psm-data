/* PlayStation(R)Mobile SDK 2.00.00
 * Copyright (C) 2014 Sony Computer Entertainment Inc.
 * All Rights Reserved.
 */

using Sce.PlayStation.HighLevel.GameEngine2D;

namespace PuzzleGameDemo
{
	public class NoCleanupScene : Scene
	{
		public override void OnEnter()
		{
			base.OnEnter();
		}

		public override void OnExit()
		{
			StopAllActions();
		}
	}

}

