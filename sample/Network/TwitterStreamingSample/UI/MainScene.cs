/* PlayStation(R)Mobile SDK 2.00.00
 * Copyright (C) 2014 Sony Computer Entertainment Inc.
 * All Rights Reserved.
 */
using System;
using System.IO;
using System.Net;
using System.Collections.Generic;
using Sce.PlayStation.HighLevel.UI;
using System.ComponentModel;
using TweetSharp;
using System.Threading;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace TwitterStreamingSample
{
    public partial class MainScene : SceneBase
    {
		public Button SetButton { get{return this.m_SetButton;}}
		public Button LogOutButton { get{return this.m_ButtonLogOut;}}
		public BusyIndicator BusyIndicator{ get{return this.m_BusyIndicator;}}

		private List<StreamInfo> StreamData = new List<StreamInfo>();
		private SetParameterDialog SetDialog = new SetParameterDialog();
		private static TwitterUser profile;
		private static TwitterUser sm_usr = null;
		private System.Timers.Timer timer = new System.Timers.Timer();
		
		public TwitterUser usr
		{
			get
			{
				if(sm_usr == null)
				{
					sm_usr = Application.Current.TwitterService.GetUserProfile(new GetUserProfileOptions());
				}
				return sm_usr;
			}
		}
		
        public MainScene()
        {
            this.InitializeWidget();
			this.BusyIndicator.Visible = false;
			
			profile = usr;
			
			this.SetButton.ButtonAction +=
				(object sender, TouchEventArgs Environment) =>
				{
					this.IsDialogShown = true;
					SetDialog.Show();
				};
			this.LogOutButton.ButtonAction +=
				(object sender, TouchEventArgs Environment) =>
				{
					this.LogOut();
				};
			this.SetParam();
        }
		
		protected override void OnShown()
		{
			base.OnShown();
		}
		
		public void SetParam()
		{
			var lockObject = new System.Object();
			
			bool invalidate = false;
			System.Timers.Timer timer = new System.Timers.Timer();
			timer.Interval = 1000;
			timer.Enabled = true;
			timer.AutoReset = true;
			timer.Elapsed += (sender, e) =>
			{
				lock (lockObject)
				{
					if (invalidate)
					{
						invalidate = false;
						Application.Current.Dispatcher.BeginInvoke(() =>
	    		       	{
							this.m_FeedsPanel.StreamList = StreamData;
						});
					}
				}
			};
			timer.Start();
			
			SetDialog.ButtonOk.ButtonAction +=
				(object sender, TouchEventArgs Environment) =>
			{
				SetDialog.Hide();
				this.IsDialogShown = false;
				
				List<string> Segment = new List<string>();
				Segment.Add(".json");
				
				Segment.Add("?follow=");
				if(profile == null)
				{
					Segment.Add("");
				}
				else
				{
					Segment.Add(profile.Id.ToString());
				}
				
				if(!String.IsNullOrWhiteSpace(SetDialog.ParamTrack.Text))
				{
					Segment.Add("&track=");
					Segment.Add(SetDialog.ParamTrack.Text);
				}
				
				string locations = "";
				if(SetDialog.ParamLocation.SelectedIndex != 0)
				{
					if(SetDialog.ParamLocation.SelectedIndex == 1)
					{
						locations = "-122.75,36.8,-121.75,37.8";
					}
					else
					{
						locations = "-74,40,-73,41";
					}
					Segment.Add("&locations=");
					Segment.Add(locations);
				}
				
				Segment.Add("&delimited=");
				Segment.Add("length");
				
				if(SetDialog.ParamWarning.SelectedIndex == 1)
				{
					Segment.Add("&stall_warnings=");
					Segment.Add("true");
				}
				
				string[] segments = Segment.ToArray();
				Application.Current.TwitterService.StreamFilter((streamEvent, response) =>
	            {
					if (response.StatusCode >= HttpStatusCode.BadRequest)
					{
						Console.WriteLine("StatusCode: " + response.StatusCode);
						Application.Current.Dispatcher.BeginInvoke(() =>
						{
							MessageDialog.CreateAndShow(MessageDialogStyle.Ok, "HTTP Error", "Error occured. Try again.");
						});
					}
					else if (response.InnerException != null)
					{
						Console.WriteLine(response.InnerException);
					}
					else if (response.StatusCode == 0)
					{
						try
						{
							JObject jo = JObject.Parse(response.Response);
	
							if (jo["user"]["id"] != null)
							{
								StreamInfo StInfo = new StreamInfo();
								
								StInfo.Id = jo["user"]["id"].ToString();
								StInfo.Name = jo["user"]["name"].ToString();
								StInfo.ScreenName = jo["user"]["screen_name"].ToString();
								StInfo.Time = jo["created_at"].ToString();
								StInfo.Text = jo["text"].ToString();
								StInfo.ProfileImgUrl = jo["user"]["profile_image_url"].ToString();
								lock (lockObject)
								{
									StreamData.Insert(0, StInfo);
									invalidate = true;
								}
							}
						}
						catch(Exception ex)
						{
							Console.WriteLine(ex);
							Console.WriteLine(response.Response);
						}
					}
					else
					{
						Console.WriteLine("StatusCode: " + response.StatusCode);
					}
	            }, segments);
				
			};
			SetDialog.ButtonCancel.ButtonAction +=
				(object sender, TouchEventArgs Environment) =>
			{
				SetDialog.Hide();
				this.IsDialogShown = false;
			};
		}
		
		public void LogOut()
		{
			if (File.Exists(TwitterStreamingSample.Settings.SETTINGS_FILE_PATH))
            {
				FileInfo file = new FileInfo(TwitterStreamingSample.Settings.SETTINGS_FILE_PATH);
				
				if((file.Attributes & FileAttributes.ReadOnly) == FileAttributes.ReadOnly)
				{
					file.Attributes = FileAttributes.Normal;
				}
				file.Delete();
			}
			Application.Current.Settings.Delete();
			Application.Current.Settings.Model.AccessToken = null;
			Application.Current.Settings.Model.RequestToken = null;
			var scene = new LoginScene();
            UISystem.SetScene(scene);
		}
    }
}
