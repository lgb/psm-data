/* PlayStation(R)Mobile SDK 2.00.00
 * Copyright (C) 2014 Sony Computer Entertainment Inc.
 * All Rights Reserved.
 */
using Sce.PlayStation.HighLevel.UI;

namespace TwitterStreamingSample
{
    public partial class WaitingDialog : Dialog
    {
        public WaitingDialog()
            : base(null, null)
        {
            InitializeWidget();
        }
    }
}
