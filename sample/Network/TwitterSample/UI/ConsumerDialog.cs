/* PlayStation(R)Mobile SDK 2.00.00
 * Copyright (C) 2014 Sony Computer Entertainment Inc.
 * All Rights Reserved.
 */
using Sce.PlayStation.HighLevel.UI;

namespace TwitterSample.UserInterface
{
    public partial class ConsumerDialog : Dialog
    {
        public Button ButtonOK { get { return this.m_ButtonOk; } }
        
        public ConsumerDialog() : base(null, null)
        {
            this.InitializeWidget();
        }
    }
}
