﻿// AUTOMATICALLY GENERATED CODE

using System;
using System.Collections.Generic;
using Sce.PlayStation.Core;
using Sce.PlayStation.Core.Imaging;
using Sce.PlayStation.Core.Environment;
using Sce.PlayStation.HighLevel.UI;

namespace TwitterSample.UserInterface
{
    partial class TitlePanel
    {
        Button m_ButtonLogin;
        Label m_LabelTitle;

        private void InitializeWidget()
        {
            InitializeWidget(LayoutOrientation.Horizontal);
        }

        private void InitializeWidget(LayoutOrientation orientation)
        {
            m_ButtonLogin = new Button();
            m_ButtonLogin.Name = "m_ButtonLogin";
            m_LabelTitle = new Label();
            m_LabelTitle.Name = "m_LabelTitle";

            // m_ButtonLogin
            m_ButtonLogin.TextColor = new UIColor(255f / 255f, 255f / 255f, 255f / 255f, 255f / 255f);
            m_ButtonLogin.TextFont = new UIFont(FontAlias.System, 25, FontStyle.Regular);

            // m_LabelTitle
            m_LabelTitle.TextColor = new UIColor(25f / 255f, 0f / 255f, 0f / 255f, 255f / 255f);
            m_LabelTitle.Font = new UIFont(FontAlias.System, 40, FontStyle.Bold);
            m_LabelTitle.TextTrimming = TextTrimming.EllipsisWord;
            m_LabelTitle.LineBreak = LineBreak.Character;
            m_LabelTitle.HorizontalAlignment = HorizontalAlignment.Center;

            // TitlePanel
            this.BackgroundColor = new UIColor(102f / 255f, 102f / 255f, 102f / 255f, 255f / 255f);
            this.Clip = true;
            this.AddChildLast(m_ButtonLogin);
            this.AddChildLast(m_LabelTitle);

            SetWidgetLayout(orientation);

            UpdateLanguage();
        }

        private LayoutOrientation _currentLayoutOrientation;
        public void SetWidgetLayout(LayoutOrientation orientation)
        {
            switch (orientation)
            {
                case LayoutOrientation.Vertical:
                    this.SetSize(544, 960);
                    this.Anchors = Anchors.None;

                    m_ButtonLogin.SetPosition(162, 635);
                    m_ButtonLogin.SetSize(214, 56);
                    m_ButtonLogin.Anchors = Anchors.None;
                    m_ButtonLogin.Visible = true;

                    m_LabelTitle.SetPosition(0, 172);
                    m_LabelTitle.SetSize(544, 88);
                    m_LabelTitle.Anchors = Anchors.None;
                    m_LabelTitle.Visible = true;

                    break;

                default:
                    this.SetSize(960, 544);
                    this.Anchors = Anchors.None;

                    m_ButtonLogin.SetPosition(370, 428);
                    m_ButtonLogin.SetSize(220, 60);
                    m_ButtonLogin.Anchors = Anchors.Height;
                    m_ButtonLogin.Visible = true;

                    m_LabelTitle.SetPosition(220, 40);
                    m_LabelTitle.SetSize(520, 80);
                    m_LabelTitle.Anchors = Anchors.None;
                    m_LabelTitle.Visible = true;

                    break;
            }
            _currentLayoutOrientation = orientation;
        }

        public void UpdateLanguage()
        {
            m_ButtonLogin.Text = UIStringTable.Get(UIStringID.RESID_LOGIN);

            m_LabelTitle.Text = "TwitterSample";
        }

        public void InitializeDefaultEffect()
        {
            switch (_currentLayoutOrientation)
            {
                case LayoutOrientation.Vertical:
                    break;

                default:
                    break;
            }
        }

        public void StartDefaultEffect()
        {
            switch (_currentLayoutOrientation)
            {
                case LayoutOrientation.Vertical:
                    break;

                default:
                    break;
            }
        }

    }
}
