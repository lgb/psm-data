/* PlayStation(R)Mobile SDK 2.00.00
 * Copyright (C) 2014 Sony Computer Entertainment Inc.
 * All Rights Reserved.
 */
using System;
using Sce.PlayStation.Core;
using Sce.PlayStation.Core.Graphics;

using Sce.PlayStation.Core.Environment;
using Sce.PlayStation.Core.Input;

using Sce.PlayStation.Framework;

namespace Sample
{

public class AppMain
{
    public static void Main(string[] args)
    {
		using( GameFrameworkSample game = new GameFrameworkSample())
		{
			game.Run(args);
		}
    }
}

}
