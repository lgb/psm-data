/* PlayStation(R)Mobile SDK 2.00.00
 * Copyright (C) 2014 Sony Computer Entertainment Inc.
 * All Rights Reserved.
 */
using System;

using Sce.PlayStation.Core;
using Sce.PlayStation.Framework;
using Tutorial.Utility;


namespace Sample
{
	public class GameActor : Actor
	{
		protected GameFrameworkSample gs;
		protected SimpleSprite sprite;
		
		public GameActor(GameFrameworkSample gs, string name) : base(name) 
		{	
			this.gs = gs;	
		}
		
		public override void Render ()
		{
			if(sprite!=null)
				sprite.Render();
			
			base.Render ();
		}
	}
}


