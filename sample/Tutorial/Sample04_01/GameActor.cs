/* PlayStation(R)Mobile SDK 2.00.00
 * Copyright (C) 2014 Sony Computer Entertainment Inc.
 * All Rights Reserved.
 */
using System;

using Sce.PlayStation.Core;
using Sce.PlayStation.Framework;


namespace Sample
{
	public class GameActor : Actor
	{
		protected GameSample gs;
		
		public GameActor(GameSample gs, string name) : base(name) 
		{	
			this.gs = gs;	
		}
	}
}


