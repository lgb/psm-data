/* PlayStation(R)Mobile SDK 2.00.00
 * Copyright (C) 2014 Sony Computer Entertainment Inc.
 * All Rights Reserved.
 */
using System;

using Sce.PlayStation.Core;
using Sce.PlayStation.Core.Graphics;
using Sce.PlayStation.Core.Environment;
using Sce.PlayStation.Core.Input;
using Sce.PlayStation.Framework;
using Tutorial.Utility;


namespace Sample
{
	public class Bullet : GameActor
	{
		static int idNum=0;
		float speed=6;
		
		Vector2 trans;
		
		public Bullet(GameFrameworkSample gs, string name,  Texture2D textrue, Vector3 position, float speed, float direction) : base (gs, name)
		{
			Name = name + idNum.ToString();
			this.sprite = new SimpleSprite( gs.Graphics, textrue);
			this.sprite.Position = position;
			this.sprite.Center.X = 0.5f;
			this.sprite.Center.Y = 0.5f;
			
			this.speed = speed;

			this.trans.X= FMath.Cos((direction-90.0f)/180*FMath.PI);
			this.trans.Y= FMath.Sin((direction-90.0f)/180*FMath.PI);
			
			idNum++;
		}

		public override void Update()
		{
			sprite.Position.X = sprite.Position.X+trans.X * speed;
			sprite.Position.Y = sprite.Position.Y+trans.Y * speed;
			
			if (sprite.Position.X < 0 || gs.rectScreen.Width < sprite.Position.X ||
				sprite.Position.Y < 0  || gs.rectScreen.Height < sprite.Position.Y)
			{
				this.Status = Actor.ActorStatus.Dead;
			}
			
			base.Update();
			
		}
		
	}
	
}

